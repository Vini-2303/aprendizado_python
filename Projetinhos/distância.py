import math

print("Vamos ver a distância entre dois pontos")

print("Digite as coordenadas do primeiro ponto")
x1 = int(input("Valor do X:"))
y1 = int(input("Valor do Y:"))

print("Digite as coordenadas do segundo ponto")
x2 = int(input("Valor do X:"))
y2 = int(input("Valor do Y:"))

distancia = math.sqrt(((x1 - x2)**2) + ((y1 - y2)**2))

if distancia >= 10:
	print("longe")

if distancia < 10:
	print("perto")
